# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = ''
  str.each_char do |chr|
    if chr.downcase != chr
      result += chr
    end
  end
  return result
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length%2 == 1
    return str[(str.length/2).floor]
  else
    return str[str.length/2 - 1] + str[str.length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char { |chr|  count += 1 if VOWELS.include?(chr)}
  return count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  i = num - 1
  while i > 0
    num = num * i
    i -= 1
  end
  return num
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  joined_string = ''
  i = 0
  while i < arr.length
    if i != arr.length - 1
      joined_string = joined_string + arr[i] + separator
    else
      joined_string = joined_string + arr[i]
    end
    i = i + 1
  end
  return joined_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str_weird = ''
  i = 1
  str.each_char do |chr|
    if i%2 == 0
      str_weird += chr.upcase
    else
      str_weird += chr.downcase
    end
    i += 1
  end
  return str_weird
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  str = str.split(' ')
  str.each_index do |i|
    if str[i].length >= 5
      str[i] = str[i].reverse
    end
  end
  return str.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  i = 1
  while i <= n
    if i%3 == 0 && i%5 == 0
      array.push('fizzbuzz')
    elsif i%3 == 0
      array.push('fizz')
    elsif i%5 == 0
      array.push('buzz')
    else
      array.push(i)
    end
  i += 1
  end
  return array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr2 = []
  arr.each do |x|
    arr2.unshift(x)
  end
  return arr2
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  i = 2
  if num == 1
    return false
  end
  while i < num
    if num%i == 0
      return false
    end
    i += 1
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  i = 1
  array = []
  while i <= num
    if num%i == 0
      array.push(i)
    end
    i += 1
  end
  return array
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  array = factors(num)
  prime_array = []
  array.each do |factors|
    if prime?(factors)
      prime_array.push(factors)
    end
  end
  return prime_array
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  return prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  count_odd = 0
  count_even = 0
  arr.each do |int|
    if int%2 == 0
      count_even += 1
    else
      count_odd += 1
    end
  end
  if count_even == 1
    arr.each do |int|
      if int%2 == 0
        return int
      end
    end
  else
    arr.each do |int|
      if int%1 == 0
        return int
      end
    end
  end
end
